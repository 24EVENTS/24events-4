package com04131.android.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.urbanairship.push.PushManager;

public class UrbanAirshipIntentReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		if (arg1.getAction().equals(PushManager.ACTION_NOTIFICATION_OPENED)) {
			for (String key : arg1.getExtras().keySet()) {
				Log.d(arg0.getPackageName(), key + " = "
						+ arg1.getExtras().get(key).toString());
			}
			arg0.startActivity(new Intent(Intent.ACTION_MAIN)
					.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
					.setClass(arg0, MainActionBarActivity.class)
					.putExtra("popup", arg1.getStringExtra("popup")));
		}
	}

}
