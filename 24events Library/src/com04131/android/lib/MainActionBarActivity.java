package com04131.android.lib;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.slidingmenu.lib.SlidingMenu;
import com.viewpagerindicator.PageIndicator;
import com04131.android.lib.fragments.dialogs.RemoveAdsIAPDialogFragment;

import java.util.Random;

public class MainActionBarActivity extends IABActivity implements OnPageChangeListener, View.OnClickListener {

    public static final int LOADER_MERKLISTE = 0;

    private SlidingMenu mSlidingMenu;

    private ViewPager mViewPager;

    public WebViewFragment mAllFragment, mDayFragment, mTwitterFragment;

    public MerklisteFragment mMerklisteFragment;

    private boolean mFinishRequestedOnce = false;

    public ActionMode actionMode;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        getSupportActionBar().setTitle(Html.fromHtml(getString(R.string.actionbar_title)));
        if ("true".equals(getString(R.string.use_custom_actionbar_title))) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setCustomView(R.layout.custom_actionbar_title);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.main_actionbar);
        setupSlidingMenu();

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new PagerAdapter());
        mViewPager.setOffscreenPageLimit(4);
        if (getIntent().getStringExtra("popup") != null) {
            showUAPopup();
        }
        if (arg0 != null && arg0.containsKey("selected_tab")) {
            mViewPager.setCurrentItem(arg0.getInt("selected_tab"));
        }
        if (getIntent().hasExtra("event")) {
            mViewPager.setCurrentItem(1);
        }
        PageIndicator indicator = (PageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);
        indicator.setOnPageChangeListener(this);
        showRatingDialog();
    }

    private void setupSlidingMenu() {
        mSlidingMenu = new SlidingMenu(this);
        mSlidingMenu.setMode(SlidingMenu.LEFT);
        mSlidingMenu.setMenu(R.layout.menu);
        mSlidingMenu.setBehindOffset((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70,
                getResources().getDisplayMetrics()));
        mSlidingMenu.setShadowWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12,
                getResources().getDisplayMetrics()));
        mSlidingMenu.setShadowDrawable(R.drawable.menu_shadow);
        mSlidingMenu.setOnClosedListener(new SlidingMenu.OnClosedListener() {
            @Override
            public void onClosed() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
        mSlidingMenu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {
            @Override
            public void onOpened() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        });
        mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        findViewById(R.id.day).setOnClickListener(this);
        findViewById(R.id.all).setOnClickListener(this);
        findViewById(R.id.twitter).setOnClickListener(this);
        findViewById(R.id.info).setOnClickListener(this);
        findViewById(R.id.merkliste).setOnClickListener(this);
        if ("false".equalsIgnoreCase(getString(R.string.enable_twitter_tab))) {
            findViewById(R.id.twitter).setVisibility(View.GONE);
        }
        setCurrentMenuItem(0);
    }

    private void showRatingDialog() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("rated", false) && new Random().nextInt() % 10 == 0) {
            new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_info).setTitle(R.string.rate_title)
                    .setMessage(R.string.rate_message).setPositiveButton(R.string.rate_title,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            prefs.edit().putBoolean("rated", true).commit();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google" +
                                    ".com/store/apps/details?id=" + getPackageName())));
                        }
                    }).setNegativeButton(R.string.cancel, null).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFinishRequestedOnce = false;
    }

    private void showUAPopup() {
        View content = getLayoutInflater().inflate(R.layout.ua_popup, null);
        WebView webView = (WebView) content.findViewById(R.id.webview);
        webView.loadData(getIntent().getStringExtra("popup"), "text/html", "UTF-8");
        new AlertDialog.Builder(this).setView(content).setNeutralButton(R.string.ok, null).show();
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int arg0) {
        setCurrentMenuItem(arg0);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.all) {
            mViewPager.setCurrentItem(1);
            if (mSlidingMenu.isMenuShowing()) {
                mSlidingMenu.toggle();
            }
        } else if (v.getId() == R.id.day) {
            mViewPager.setCurrentItem(0);
            if (mSlidingMenu.isMenuShowing()) {
                mSlidingMenu.toggle();
            }
        } else if (v.getId() == R.id.twitter) {
            mViewPager.setCurrentItem(3);
            if (mSlidingMenu.isMenuShowing()) {
                mSlidingMenu.toggle();
            }
        } else if (v.getId() == R.id.info) {
            mViewPager.setCurrentItem(4);
            if (mSlidingMenu.isMenuShowing()) {
                mSlidingMenu.toggle();
            }
        } else if (v.getId() == R.id.merkliste) {
            mViewPager.setCurrentItem(2);
            if (mSlidingMenu.isMenuShowing()) {
                mSlidingMenu.toggle();
            }
        }
    }

    private void setCurrentMenuItem(int currentPage) {
        findViewById(R.id.all).setSelected(currentPage == 1);
        findViewById(R.id.day).setSelected(currentPage == 0);
        findViewById(R.id.merkliste).setSelected(currentPage == 2);
        if ("false".equalsIgnoreCase(getString(R.string.enable_twitter_tab))) {
            findViewById(R.id.info).setSelected(currentPage == 3);
        } else {
            findViewById(R.id.twitter).setSelected(currentPage == 3);
            findViewById(R.id.info).setSelected(currentPage == 4);
        }
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public Fragment getItem(int arg0) {
            if (arg0 == 0) {
                Bundle args = new Bundle();
                args.putString("url", getString(R.string.url_days));
                mDayFragment = new WebViewFragment();
                mDayFragment.setArguments(args);
                return mDayFragment;
            } else if (arg0 == 1) {
                Bundle args = new Bundle();
                args.putString("url", getString(R.string.url_all));
                if (getIntent().hasExtra("event")) {
                    Event event = (Event) getIntent().getSerializableExtra("event");
                    Log.d(getPackageName(), "Loading event: " + event);
                    String url = getString(R.string.url_event, event.id);
                    args.putString("url2", url);
                }
                mAllFragment = new WebViewFragment();
                mAllFragment.setArguments(args);
                return mAllFragment;
            } else if (arg0 == 2) {
                if (mMerklisteFragment == null) {
                    mMerklisteFragment = MerklisteFragment.newInstance();
                }
                return mMerklisteFragment;
            } else if (arg0 == 4) {
                return new InfoFragment();
            } else if (arg0 == 3) {
                if ("true".equals(getString(R.string.enable_twitter_tab))) {
                    Bundle args = new Bundle();
                    args.putString("url", getString(R.string.url_twitter));
                    mTwitterFragment = new WebViewFragment();
                    mTwitterFragment.setArguments(args);
                    return mTwitterFragment;
//                    return new CustomMapFragment();
                } else {
                    return new InfoFragment();
                }
            } else {
                return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return getString(R.string.menu_day).toUpperCase();
            } else if (position == 1) {
                return getString(R.string.menu_all).toUpperCase();
            } else if (position == 4) {
                return getString(R.string.menu_info).toUpperCase();
            } else if (position == 3) {
                if ("true".equals(getString(R.string.enable_twitter_tab))) {
                    return getString(R.string.menu_twitter).toUpperCase();
                } else {
                    return getString(R.string.menu_info).toUpperCase();
                }
            } else if (position == 2) {
                return getString(R.string.menu_merkliste).toUpperCase();
            } else {
                return "";
            }
        }

        @Override
        public int getCount() {
            if ("true".equals(getString(R.string.enable_twitter_tab))) {
                return 5;
            } else {
                return 4;
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getSupportMenuInflater().inflate(R.menu.main, menu);
            return true;
        } catch (Exception e) {
            Log.e(MainActionBarActivity.class.getSimpleName(), "Error inflating menu", e);
            return false;
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (getString(R.string.iab_public_key).length() == 0) {
            menu.removeItem(R.id.remove_ads);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.quit) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName
                    ());
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_chooser_title,
                    getString(R.string.app_name))));
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            mSlidingMenu.toggle();
            return true;
        } else if (item.getItemId() == R.id.remove_ads) {
            RemoveAdsIAPDialogFragment.newInstance().show(getSupportFragmentManager(), "iap");
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
            if (mSlidingMenu.isMenuShowing()) {
                mSlidingMenu.toggle();
            } else if (mViewPager.getCurrentItem() == 3) {
                requestBackKeyFinish();
            } else if (mViewPager.getCurrentItem() == 1) {
                if (mAllFragment != null && mAllFragment.webView != null && mAllFragment.webView.canGoBack()) {
                    mAllFragment.webView.goBack();
                } else {
                    requestBackKeyFinish();
                }
            } else if (mViewPager.getCurrentItem() == 0) {
                if (mDayFragment != null && mDayFragment.webView != null && mDayFragment.webView.canGoBack()) {
                    mDayFragment.webView.goBack();
                } else {
                    requestBackKeyFinish();
                }
            } else if (mViewPager.getCurrentItem() == 2) {
                if ("true".equals(getString(R.string.enable_twitter_tab))) {
                    if (mTwitterFragment != null && mTwitterFragment.webView != null && mTwitterFragment.webView
                            .canGoBack()) {
                        mTwitterFragment.webView.goBack();
                    } else {
                        requestBackKeyFinish();
                    }
                } else {
                    requestBackKeyFinish();
                }
            } else {
                requestBackKeyFinish();
            }
            return true;
        } else {
            return false;
        }
    }

    private void requestBackKeyFinish() {
        if (mFinishRequestedOnce) {
            finish();
        } else {
            mFinishRequestedOnce = true;
            Toast.makeText(this, R.string.quit_hint, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mFinishRequestedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selected_tab", mViewPager.getCurrentItem());
    }

}
