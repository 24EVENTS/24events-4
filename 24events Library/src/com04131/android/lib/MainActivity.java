package com04131.android.lib;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

public class MainActivity extends TabActivity implements OnTabChangeListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		setupTabs();
		findViewById(R.id.header_info).setVisibility(View.GONE);
		findViewById(R.id.header_logo).setVisibility(View.VISIBLE);
		findViewById(R.id.header_logo).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						getTabHost().setCurrentTabByTag("day");
					}
				});
		getTabHost().setOnTabChangedListener(this);
		if (getIntent().getStringExtra("popup") != null) {
			showUAPopup();
		}
	}

	private void showUAPopup() {
		View content = getLayoutInflater().inflate(R.layout.ua_popup, null);
		WebView webView = (WebView) content.findViewById(R.id.webview);
		webView.loadData(getIntent().getStringExtra("popup"), "text/html",
				"UTF-8");
		new AlertDialog.Builder(this).setView(content)
				.setNeutralButton(R.string.ok, null).show();
	}

	private void setupTabs() {
		TabHost tabHost = getTabHost();
		TabSpec tabSpec = tabHost.newTabSpec("day");
		tabSpec.setIndicator(getString(R.string.day_tab_title), getResources()
				.getDrawable(R.drawable.icon_time));
		tabSpec.setContent(new Intent(this, WebViewTabActivity.class).putExtra(
				"url", getString(R.string.url_days)));
		// "url", "http://goddchen.de/test/04131-api/cal.php"));
		tabHost.addTab(tabSpec);
		tabSpec = tabHost.newTabSpec("all");
		tabSpec.setIndicator(getString(R.string.all_events_tab_title),
				getResources().getDrawable(R.drawable.icon_calendar));
		Intent allIntent = new Intent(this, WebViewTabActivity.class).putExtra(
				"url", getString(R.string.url_all));
		if (getIntent().hasExtra("event")) {
			Event event = (Event) getIntent().getSerializableExtra("event");
			Log.d(getPackageName(), "Loading event: " + event);
			String url = getString(R.string.url_event, event.id);
			allIntent.putExtra("url2", url);
		}
		tabSpec.setContent(allIntent);
		tabHost.addTab(tabSpec);
		tabSpec = tabHost.newTabSpec("info");
		tabSpec.setIndicator(getString(R.string.info_tab_title), getResources()
				.getDrawable(R.drawable.icon_information));
		tabSpec.setContent(new Intent(this, InfoActivity.class));
		tabHost.addTab(tabSpec);
		TabWidget tabWidget = getTabWidget();
		for (int i = 0; i < tabWidget.getChildCount(); i++) {
			View tab = tabWidget.getChildAt(i);
			tab.setBackgroundResource(R.drawable.tab_background);
			((TextView) tab.findViewById(android.R.id.title))
					.setTextColor(getResources().getColorStateList(
							R.color.tab_text));
		}
		if (getIntent().hasExtra("event")) {
			getTabHost().setCurrentTabByTag("all");
		}
	}

	@Override
	public void onTabChanged(String tabId) {
		if ("day".equals(tabId) || "all".equals(tabId)) {
			findViewById(R.id.header_logo).setVisibility(View.VISIBLE);
			findViewById(R.id.header_info).setVisibility(View.GONE);
		} else if ("info".equals(tabId)) {
			findViewById(R.id.header_logo).setVisibility(View.GONE);
			findViewById(R.id.header_info).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		try {
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		} catch (Exception e) {
			Log.w(getClass().getSimpleName(), "Error inflating menu", e);
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.quit) {
			finish();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

}